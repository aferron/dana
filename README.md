# Dana
Raising awareness of the impacts of eating habits on the environment: a data visualization exploration

## Author 

Aymeric Ferron

## Code

Code workflow and architecture :

[Code_UML](Documentation/Dana_code-Callflow.pdf)

## Current version

V2.1.3 : [Download build](Build/V2.1.3_FINAL.zip) (Windows)

## Past version

V2.1.2 ; Outdated environmental data : [Download build](Build/V2.1.2_FINAL.zip) (Windows)


## Research report

Available on HAL : [Une exploration de visualisation de données pour sensibiliser à l’impact environnemental des régimes alimentaires](https://hal.science/hal-04129393)



