using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class containing the data computation utilities.
/// It computes and exposes environmental indicators for one year and five days of eating a specific diet
/// based on Agribalyse database. 
/// </summary>
public class AgribalyseDataComputer
{
    [SerializeField] private int worldNumberAssociated = -1;

    /// <summary>
    /// Contains percentages input for each type of food for the current computation.
    /// </summary>
    private Dictionary<FoodEnum, float> foodPercentagesInput = new Dictionary<FoodEnum, float>();

    /// <summary>
    /// Exposes the current percentage sum from inputs. 
    /// </summary>
    public int currentSumPercentages = 0;

    /// <summary>
    /// Contains detailed food GHG emissions for each type of food based on the current consumption percentage inputs.
    /// </summary>
    private Dictionary<FoodEnum, float> foodGHGEmissions = new Dictionary<FoodEnum, float>();

    /// <summary>
    /// Contains detailed food land usage for each type of food based on the current consumption percentage inputs.
    /// </summary>
    private Dictionary<FoodEnum, float> foodLandUsed = new Dictionary<FoodEnum, float>();

    /// <summary>
    /// Contains detailed food water usage for each type of food based on the current consumption percentage inputs.
    /// </summary>
    private Dictionary<FoodEnum, float> foodWaterUsed = new Dictionary<FoodEnum, float>();

    /// <summary>
    /// Exposes the total impact of the current chosen diet on land for a five-days consumption.
    /// </summary>
    public float totalFoodLandUsedInFiveDays = 0;

    /// <summary>
    /// Exposes the total impact of the current chosen diet on water for a five-days consumption.
    /// </summary>
    public float totalFoodWaterUsedInFiveDays = 0;

    /// <summary>
    /// Exposes the total impact of the current chosen diet on GHG for a five-days consumption.
    /// </summary>
    public float totalFoodGHGEmissionsInFiveDays = 0;

    /// <summary>
    /// Exposes the total impact of the current chosen diet on land for a year consumption.
    /// </summary>
    public float totalFoodLandUsedInOneYear = 0;

    /// <summary>
    /// Exposes the total impact of the current chosen diet on water for a year consumption.
    /// </summary>
    public float totalFoodWaterUsedInOneYear = 0;

    /// <summary>
    /// Exposes the total impact of the current chosen diet on GHG for a year consumption.
    /// </summary>
    public float totalFoodGHGEmissionsInOneYear = 0;

    /// <summary>
    /// Mass of a portion during a meal. Equals to 0.120 kilograms.
    /// </summary>
    private float massPortion = 0.120f;

    /// <summary>
    /// Number of meal calculated based on required inputs.
    /// Currently, user is asked to submit 10 meals, put another way 2 meals (lunch and dinner) during 5 days.
    /// </summary>
    private float numberOfMealInOneComputation = 10;

    /// <summary>
    /// Number of portion used for one computation.
    /// A balanced meal contains 5 portions of food : proteins, vegetables, fruits, starch, dairy.
    /// </summary>
    private float
        numberOfPortionInOneComputation = 0;

    /// <summary>
    /// Class constructor.
    /// </summary>
    public AgribalyseDataComputer(int miniWorldAssociated)
    {
        // A balanced meal contains 5 portions of food : proteins, vegetables, fruits, starch, dairy.
        numberOfPortionInOneComputation = numberOfMealInOneComputation * 5;
        worldNumberAssociated = miniWorldAssociated;
    }

    /// <summary>
    /// Computes food data consequences on environment for five days and one year.
    /// This computation is independent from a Dana perspective.
    /// If computation went well, calls module for Dana's data refresh and output refresh.
    /// </summary>
    /// <returns>True if computation has been possible, false otherwise.</returns>
    public void ComputeFoodInput()
    {
        // Clear all the current data
        ResetCurrentDataComputed();

        // Get all the enum values
        Array enumArray = Enum.GetValues(typeof(FoodEnum));

        // Get all the percentages input, convert it in float and get the sum of it
        currentSumPercentages = 0;
        foreach (FoodEnum enumValue in enumArray)
        {
            if (enumValue != FoodEnum.Unknown)
            {
                foodPercentagesInput.Add(
                    enumValue, GameManager.Instance.dataInputList[worldNumberAssociated].GetPercentageWithKey(enumValue) / 100f);
                currentSumPercentages += GameManager.Instance.dataInputList[worldNumberAssociated].GetPercentageWithKey(enumValue);
            }
        }

        // For each food entry, computes its consequences on environment weighted by its percentage
        foreach (KeyValuePair<FoodEnum, float> item in foodPercentagesInput)
        {
            FoodDataEntry currentFoodDataEntry = null;
            GameManager.Instance.myFoodDataCollection.CurrentFoodDataCollection.TryGetValue(
                item.Key, out currentFoodDataEntry);

            if (currentFoodDataEntry != null)
            {
                foodGHGEmissions.Add(
                    item.Key,
                    item.Value * numberOfPortionInOneComputation * currentFoodDataEntry.kgCO2PerKg * massPortion);
                totalFoodGHGEmissionsInFiveDays +=
                    item.Value * numberOfPortionInOneComputation * currentFoodDataEntry.kgCO2PerKg *
                    massPortion;

                foodLandUsed.Add(
                    item.Key,
                    item.Value * numberOfPortionInOneComputation * currentFoodDataEntry.surfacePerKg * massPortion);
                totalFoodLandUsedInFiveDays +=
                    item.Value * numberOfPortionInOneComputation * currentFoodDataEntry.surfacePerKg *
                    massPortion;

                foodWaterUsed.Add(
                    item.Key,
                    item.Value * numberOfPortionInOneComputation * currentFoodDataEntry.waterPerKg * massPortion);
                totalFoodWaterUsedInFiveDays +=
                    item.Value * numberOfPortionInOneComputation * currentFoodDataEntry.waterPerKg *
                    massPortion;
            }
            else
            {
                Debug.LogError("No food data entry found for key : " + item.Key);
                return;
            }
        }
        
        // Now that the environmental impact has been computed for 5 days, converts it in one year.
        ComputeFiveDaysDataInOneYear();

        // Calls the simulation data converter in order to convert current data in relevant data for the simulation.  
        GameManager.Instance.miniWorldDataConverterList[worldNumberAssociated].RefreshMiniWorldData();
        
        // Calls the output management to update output values in UI.
        GameManager.Instance.dataOutputList[worldNumberAssociated].RefreshOutputValuesFromAgribalyse();
    }

    /// <summary>
    /// Resets all the data used for the computation and exposed data.
    /// </summary>
    private void ResetCurrentDataComputed()
    {
        // Clear out the collections
        foodPercentagesInput.Clear();
        foodGHGEmissions.Clear();
        foodLandUsed.Clear();
        foodWaterUsed.Clear();

        // Clear data sum
        totalFoodLandUsedInFiveDays = 0;
        totalFoodWaterUsedInFiveDays = 0;
        totalFoodGHGEmissionsInFiveDays = 0;
    }

    /// <summary>
    /// Computes five-day-diet-impacts in a one-year-diet-impacts.
    /// </summary>
    private void ComputeFiveDaysDataInOneYear()
    {
        totalFoodLandUsedInOneYear = totalFoodLandUsedInFiveDays         * 73;
        totalFoodWaterUsedInOneYear = totalFoodWaterUsedInFiveDays       * 73;
        totalFoodGHGEmissionsInOneYear = totalFoodGHGEmissionsInFiveDays * 73;
    }

    /// <summary>
    /// Function allowing to reset all the current computed values.
    /// Relevant to call when all the inputs are reset. 
    /// </summary>
    public void ResetDataValueComputation()
    {
        foodPercentagesInput.Clear();
        currentSumPercentages = 0;
        foodLandUsed.Clear();
        foodWaterUsed.Clear();
        foodGHGEmissions.Clear();
        totalFoodLandUsedInFiveDays = 0;
        totalFoodLandUsedInOneYear = 0;
        totalFoodWaterUsedInFiveDays = 0;
        totalFoodWaterUsedInOneYear = 0;
        totalFoodGHGEmissionsInFiveDays = 0;
        totalFoodGHGEmissionsInOneYear = 0;
    }
}