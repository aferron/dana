/// <summary>
/// A food enum is created for each type of food used in the simulation.
/// If a new type of food is used in Dana, please add new enum corresponding. 
/// </summary>
public enum FoodEnum
{
    Beef,
    Fish,
    Pork,
    Chicken,
    Fruits,
    Vegetables,
    PlantBasedProteins,
    Pasta,
    Dairy,

    // Add new Enum above if necessary.
    Unknown,
}