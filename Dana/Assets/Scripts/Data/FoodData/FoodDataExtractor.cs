using UnityEngine;

/// <summary>
/// Class managing the extraction of food data from a JSON file.
/// It stores data in the myRawFoodDataCollection attribute. 
/// </summary>
public class FoodDataExtractor : MonoBehaviour
{
    /// <summary>
    /// Link toward the JSON file (dragged and dropped in Unity).
    /// </summary>
    [SerializeField] private TextAsset JSONFile = null;

    /// <summary>
    /// Data stored from the JSON file.
    /// </summary>
    public RawFoodDataCollection myRawFoodDataCollection = null;

    /// <summary>
    /// Extract data from a JSON file.
    /// </summary>
    public void ExtractData()
    {
        if (JSONFile != null)
        {
            myRawFoodDataCollection = new RawFoodDataCollection();
            // Read the file and construct the collection
            myRawFoodDataCollection = JsonUtility.FromJson<RawFoodDataCollection>(JSONFile.text);
        }
        else
        {
            Debug.LogError("JSONFile is null");
        }
    }
}