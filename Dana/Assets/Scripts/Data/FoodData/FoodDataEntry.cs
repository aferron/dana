/// <summary>
/// Data class containing data for each food item.
/// </summary>
[System.Serializable]
public class FoodDataEntry
{
    /// <summary>
    /// Name of the food (overall type of food).
    /// </summary>
    public string foodName = null;

    /// <summary>
    /// More precise description of the food.
    /// </summary>
    public string foodDescription = null;

    /// <summary>
    /// Mass of food consumed per meal. Value is taken from Edo (Sauvé et al.).
    /// </summary>
    public float massConsumedPerMeal = 0;

    /// <summary>
    /// Mass of CO2 in kilograms emitted for 1 kilogram of food consumption.
    /// Value taken in Agrybalise.
    /// </summary>
    public float kgCO2PerKg = 0;

    /// <summary>
    /// Mass of CO2 in kilograms emitted for one potion of food during a meal.
    /// </summary>
    public float kgCO2PerMeal = 0;

    /// <summary>
    /// Land used for 1 kilogram of food consumption.
    /// Dimensionless indicator (Pt) taken in Agrybalise.
    /// </summary>
    public float surfacePerKg = 0;

    /// <summary>
    /// Land used for one portion of food during meal (in Pt).
    /// </summary>
    public float surfacePerMeal = 0;

    /// <summary>
    /// Water used for 1 kilogram of food consumption (in cubic meters).
    /// </summary>
    public float waterPerKg = 0;

    /// <summary>
    /// Water used for one portion of food during a meal (in cubic meter).
    /// </summary>
    public float waterPerMeal = 0;

    /// <summary>
    /// URL toward the food item in Agrybalise database.
    /// </summary>
    public string reference = null;
}