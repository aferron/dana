/// <summary>
/// Data class containing an array of food entry.
/// </summary>
[System.Serializable]
public class RawFoodDataCollection
{
    public FoodDataEntry[] foodEntries = null;
}