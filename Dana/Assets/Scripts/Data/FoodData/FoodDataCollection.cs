using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Data class managing and exposing food data.
/// </summary>
[System.Serializable]
public class FoodDataCollection
{
    /// <summary>
    /// Dictionary containing all the data food. 
    /// </summary>
    public Dictionary<FoodEnum, FoodDataEntry> CurrentFoodDataCollection = new Dictionary<FoodEnum, FoodDataEntry>();

    /// <summary>
    /// Extractor instance allowing to extract data from JSON.
    /// </summary>
    [SerializeField] private FoodDataExtractor dataExtractor = null;

    /// <summary>
    /// Call food data extraction from the JSON and fill the dictionary with it for Dana usage.
    /// If a new food is added, requires to add a case in the switch. 
    /// </summary>
    public void initFoodData()
    {
        dataExtractor.ExtractData();

        // Stock data in the dictionary 
        foreach (var foodEntry in dataExtractor.myRawFoodDataCollection.foodEntries)
        {
            // Adding a new type of food requires to add a new Enum and a new case in the switch
            switch (foodEntry.foodName)
            {
                case "beef":
                    CurrentFoodDataCollection.Add(FoodEnum.Beef, foodEntry);
                    break;
                case "chicken":
                    CurrentFoodDataCollection.Add(FoodEnum.Chicken, foodEntry);
                    break;
                case "fish":
                    CurrentFoodDataCollection.Add(FoodEnum.Fish, foodEntry);
                    break;
                case "pork":
                    CurrentFoodDataCollection.Add(FoodEnum.Pork, foodEntry);
                    break;
                case "vegetables":
                    CurrentFoodDataCollection.Add(FoodEnum.Vegetables, foodEntry);
                    break;
                case "fruits":
                    CurrentFoodDataCollection.Add(FoodEnum.Fruits, foodEntry);
                    break;
                case "pasta":
                    CurrentFoodDataCollection.Add(FoodEnum.Pasta, foodEntry);
                    break;
                case "plant-based protein":
                    CurrentFoodDataCollection.Add(FoodEnum.PlantBasedProteins, foodEntry);
                    break;
                case "dairy":
                    CurrentFoodDataCollection.Add(FoodEnum.Dairy, foodEntry);
                    break;
                default:
                    Debug.LogWarning("Unknown food spotted during food data saving : " + foodEntry.foodName +
                                     ". Saved as Unknown in enum.");
                    CurrentFoodDataCollection.Add(FoodEnum.Unknown, foodEntry);
                    break;
            }
        }

        // Calculate ecological data for a meal 
        foreach (KeyValuePair<FoodEnum, FoodDataEntry> foodItem in CurrentFoodDataCollection)
        {
            foodItem.Value.surfacePerMeal = foodItem.Value.surfacePerKg * foodItem.Value.massConsumedPerMeal;
            foodItem.Value.waterPerMeal = foodItem.Value.waterPerKg     * foodItem.Value.massConsumedPerMeal;
            foodItem.Value.kgCO2PerMeal = foodItem.Value.kgCO2PerKg     * foodItem.Value.massConsumedPerMeal;
        }
    }
}