using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// Class managing all the cameras in the mini-world.
/// Allow to change from point of view.
/// </summary>
public class CamerasManager : MonoBehaviour
{
    /// <summary>
    /// Camera above the mini-world with a general view.
    /// </summary>
    [SerializeField] private Camera mainCamera = null;

    private Vector3 initialPosition = Vector3.zero;
    private Quaternion initialRotation = Quaternion.identity;

    /// <summary>
    /// Camera showing the village near the lake.
    /// </summary>
    [FormerlySerializedAs("lakeVillageCamera")] [SerializeField]
    private Transform lakeVillageTransform = null;

    /// <summary>
    /// Plan allowing occlusion when ocean recover the village.
    /// </summary>
    [SerializeField] private GameObject oceanPlaneForVillage = null;

    /// <summary>
    /// Camera showing the forest.
    /// </summary>
    [FormerlySerializedAs("forestCamera")] [SerializeField]
    private Transform forestCameraTransform = null;

    /// <summary>
    /// Plan allowing occlusion when ocean recover the forest.
    /// </summary>
    [SerializeField] private GameObject oceanPlaneForForest = null;

    /// <summary>
    /// Camera showing the island.
    /// </summary>
    [FormerlySerializedAs("islandCamera")] [SerializeField]
    private Transform islandCameraTransform = null;

    /// <summary>
    /// Plan allowing occlusion when ocean recover the island.
    /// </summary>
    [SerializeField] private GameObject oceanPlaneForIsland = null;

    /// <summary>
    /// Camera showing the beach.
    /// </summary>
    [FormerlySerializedAs("beachCamera")] [SerializeField]
    private Transform beachCameraTransform = null;

    /// <summary>
    /// Plan allowing occlusion when ocean recover the beach.
    /// </summary>
    [SerializeField] private GameObject oceanPlaneForBeach = null;

    /// <summary>
    /// Camera showing the mountain.
    /// </summary>
    [FormerlySerializedAs("mountainCamera")] [SerializeField]
    private Transform mountainCameraTransform = null;

    /// <summary>
    /// GameObject of the see.
    /// </summary>
    [SerializeField] private GameObject see = null;

    private Camera UICamera = null;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera.enabled = true;
        initialPosition = mainCamera.transform.position;
        initialRotation = mainCamera.transform.rotation;

        UICamera = gameObject.GetComponentInChildren<Camera>();
        UICamera.enabled = true;
    }

    private void Update()
    {
        // Activate camera's village occlusion if required
        if (see.transform.localPosition.y >= 19.7)
        {
            oceanPlaneForVillage.SetActive(true);
        }
        else
        {
            oceanPlaneForVillage.SetActive(false);
        }

        // Activate forest's village occlusion if required
        if (see.transform.localPosition.y >= 13.1)
        {
            oceanPlaneForForest.SetActive(true);
        }
        else
        {
            oceanPlaneForForest.SetActive(false);
        }

        // Activate camera's beach occlusion if required
        if (see.transform.localPosition.y >= 17.6)
        {
            oceanPlaneForBeach.SetActive(true);
            oceanPlaneForIsland.SetActive(true);
        }
        else
        {
            oceanPlaneForBeach.SetActive(false);
            oceanPlaneForIsland.SetActive(false);
        }
    }

    /// <summary>
    /// Show top-down view.
    /// </summary>
    public void ShowMainView()
    {
        mainCamera.transform.SetPositionAndRotation(initialPosition, initialRotation);
    }

    /// <summary>
    /// Show village near the lake.
    /// </summary>
    public void ShowLakeVillage()
    {
        mainCamera.transform.SetPositionAndRotation(lakeVillageTransform.position, 
                                                    lakeVillageTransform.rotation);
    }

    /// <summary>
    /// Show forest.
    /// </summary>
    public void ShowForest()
    {
        mainCamera.transform.SetPositionAndRotation(forestCameraTransform.position,
                                                    forestCameraTransform.rotation);
    }

    /// <summary>
    /// Show island.
    /// </summary>
    public void ShowIsland()
    {
        mainCamera.transform.SetPositionAndRotation(islandCameraTransform.position,
                                                    islandCameraTransform.rotation);
    }

    /// <summary>
    /// Show beach.
    /// </summary>
    public void ShowBeach()
    {
        mainCamera.transform.SetPositionAndRotation(beachCameraTransform.position,
                                                    beachCameraTransform.rotation);
    }

    /// <summary>
    /// Show mountain.
    /// </summary>
    public void ShowMountain()
    {
        mainCamera.transform.SetPositionAndRotation(mountainCameraTransform.position,
                                                    mountainCameraTransform.rotation);
    }
}