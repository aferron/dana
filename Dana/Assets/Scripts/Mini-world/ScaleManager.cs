using UnityEngine;

/// <summary>
/// Class managing the scale of a gameObject.
/// Mainly used in Dana for glaciers.
/// </summary>
public class ScaleManager : MonoBehaviour
{
    /// <summary>
    /// Transform of the GameObject to manage
    /// </summary>
    [SerializeField] private Transform goTransform = null;

    /// <summary>
    /// Initial scale of the GameObject
    /// </summary>
    private Vector3 initialScale = Vector3.zero;

    /// <summary>
    /// Max scale the object could have
    /// </summary>
    private Vector3 scaleMax = Vector3.zero;

    /// <summary>
    /// Min scale the object could have
    /// </summary>
    private Vector3 scaleMin = Vector3.zero;
    
    void Awake()
    {
        initialScale = goTransform.localScale;
        scaleMax = initialScale;
    }

    public void ResetScale()
    {
        goTransform.localScale = initialScale;
    }

    /// <summary>
    /// Set new scale of the gameObject according to the percentage given.
    /// </summary>
    /// <param name="modificationPercentage">Modification percentage</param>
    public void SetScale(float modificationPercentage)
    {
        Vector3 futureScale = initialScale;
        futureScale *= modificationPercentage;
        if (IsNewScaleValid(futureScale))
        {
            goTransform.localScale = futureScale;
        }
    }

    /// <summary>
    /// Check if the new scale is valid according to max and min values possible.
    /// </summary>
    /// <param name="newScale">True if it's valid, false otherwise.</param>
    /// <returns></returns>
    private bool IsNewScaleValid(Vector3 newScale)
    {
        return newScale.x >= scaleMin.x &&
               newScale.y >= scaleMin.y &&
               newScale.z >= scaleMin.z;
    }
}