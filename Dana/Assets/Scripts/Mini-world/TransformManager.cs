using UnityEngine;

/// <summary>
/// Class managing transform modification of the water (lake and see) and the ice.
/// It is used as a singleton.
/// </summary>
public class TransformManager : MonoBehaviour
{
    /// <summary>
    /// Movement manager is a singleton. Instance properties allow to access it.
    /// </summary>
    public static            TransformManager Instance;
    
    /// <summary>
    /// HeightManager managing the see of Dana.
    /// </summary>
    [SerializeField] private HeightManager   seeLevel;
    
    /// <summary>
    /// HeightManager managing the lake of Dana.
    /// </summary>
    [SerializeField] private HeightManager   lakeLevel;

    [SerializeField] private ScaleManager glacier1 = null;
    [SerializeField] private ScaleManager glacier2 = null;
    [SerializeField] private ScaleManager glacier3 = null;
    [SerializeField] private ScaleManager glacier4 = null;
    
    
    // Start is called before the first frame update
    void Start()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);


        Instance = this;
    }
    
    /***************
     * See manager *
     **************/

    /// <summary>
    /// Increase see level by heightChangeMagnitude.
    /// </summary>
    /// <param name="heightChangeMagnitude">Magnitude of the elevation on y axis</param>
    public void SeeIncrease(float heightChangeMagnitude)
    {
        seeLevel.IncreaseHeight(heightChangeMagnitude);
    }

    /// <summary>
    /// Decrease see level by heightChangeMagnitude.
    /// </summary>
    /// <param name="heightChangeMagnitude">Magnitude of the drop on y axis</param>
    public void SeeDecrease(float heightChangeMagnitude)
    {
        seeLevel.DecreaseHeight(heightChangeMagnitude);
    }
    
    /// <summary>
    /// Set see level to height.
    /// </summary>
    /// <param name="height">New position of the see on the y axis.</param>
    public void SetSeeLevel(float height)
    {
        seeLevel.SetHeight(height + seeLevel.initialHeightPosition);
    }
    
    /// <summary>
    /// Reset see to its initial position.
    /// </summary>
    public void ResetSee()
    {
        seeLevel.ResetPosition();
    }
    
    /****************
     * Lake manager *
     ***************/

    /// <summary>
    /// Increase lake level by heightChangeMagnitude.
    /// </summary>
    /// <param name="heightChangeMagnitude">Magnitude of the elevation on y axis</param>
    public void LakeIncrease(float heightChangeMagnitude)
    {
        lakeLevel.IncreaseHeight(heightChangeMagnitude);
    }

    
    /// <summary>
    /// Decrease lake level by heightChangeMagnitude.
    /// </summary>
    /// <param name="heightChangeMagnitude">Magnitude of the drop on y axis</param>
    public void LakeDecrease(float heightChangeMagnitude)
    {
        lakeLevel.DecreaseHeight(heightChangeMagnitude);
    }
    
    /// <summary>
    /// Set lake level to height.
    /// </summary>
    /// <param name="height">New position of the see on the y axis.</param>
    public void SetLakeLevel(float height)
    {
        lakeLevel.SetHeight(height);
    }
    
    public void SetLakeLevelOnMin()
    {
        lakeLevel.SetOnMin();
    }

    /// <summary>
    /// Reset lake to its initial position.
    /// </summary>
    public void ResetLakeLevel()
    {
        lakeLevel.ResetPosition();
    }

    /******************
     * GlacierManager *
     *****************/

    /// <summary>
    /// Apply a scale change on all the glaciers depending on the given percentage.
    /// Could be an augmentation (percentage > 1) or a reduction (percentage less than 0).
    /// </summary>
    /// <param name="percentage">Change percentage apply to the initial scale of the glacier</param>
    public void SetGlaciers(float percentage)
    {
        glacier1.SetScale(percentage);
        glacier2.SetScale(percentage);
        glacier3.SetScale(percentage);
        glacier4.SetScale(percentage);
    }

    /// <summary>
    /// Resets glaciers scale.
    /// </summary>
    public void ResetGlacier()
    {
        glacier1.ResetScale();
        glacier2.ResetScale();
        glacier3.ResetScale();
        glacier4.ResetScale();
    }
    
}