using UnityEngine;

/// <summary>
/// Class linking a wheat field with a tree.
/// Allow to display only one in the same time and create an illusion of deforestation.
/// </summary>
public class ForestManager : MonoBehaviour
{
    [SerializeField] private GameObject tree = null;

    [SerializeField] private GameObject wheat = null;

    /// <summary>
    /// Set tree active and unable wheat field corresponding. 
    /// </summary>
    public void SetTreeActive()
    {
        tree.SetActive(true);
        wheat.SetActive(false);
    }

    /// <summary>
    /// Set wheat field active and unable tree corresponding. 
    /// </summary>
    public void SetWheatActive()
    {
        tree.SetActive(false);
        wheat.SetActive(true);
    }

    /// <summary>
    /// Set tree non inactive if the connected wheat is currently activated.
    /// </summary>
    public void HideTreeIfWheatIsActive()
    {
        if (wheat.activeSelf)
        {
            tree.SetActive(false);
        }
    }
}
