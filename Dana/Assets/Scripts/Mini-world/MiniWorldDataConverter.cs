using Unity.VisualScripting;
using UnityEngine;

/// <summary>
/// Class converting Agribalyse data in relevant data for the mini-world.
/// It contextualizes data with the mini-world function, such as GHG sink, water replenishment, and time.  
/// </summary>
public class MiniWorldDataConverter : MonoBehaviour
{
    [SerializeField] private int worldNumberAssociated = -1;
    
    /// <summary>
    /// Property giving access to the current amount of CO2 in Dana.
    /// </summary>
    public float CurrentAmountOfCO2
    {
        get { return currentAmountOfCO2; }
        private set { currentAmountOfCO2 = value; }
    }

    private float currentAmountOfCO2 = 0;

    /// <summary>
    /// Chosen value for the carbon sink of Dana during one year. 
    /// </summary>
    private float CO2SinkAnnualValue = 600;

    /// <summary>
    /// Property giving access to the current amount of fresh water in Dana.
    /// </summary>
    public float CurrentAmountOfFreshWater
    {
        get { return currentAmountOfFreshWater; }
        private set { currentAmountOfFreshWater = value; }
    }

    private float currentAmountOfFreshWater = 0;

    /// <summary>
    /// Chosen value for the annual fresh water replenishment in Dana.
    /// </summary>
    private float annualReplenishment = 2011;

    /// <summary>
    /// Chosen value for the initial quantity of water in Dana.
    /// </summary>
    public float initialFreshWaterVolume = 20110;

    /// <summary>
    /// Property given access to the current percentage of land used in Dana.
    /// </summary>
    public float CurrentPercentageOfLandUsed
    {
        get { return currentPercentageOfLandUsed; }
        private set { currentPercentageOfLandUsed = value; }
    }

    private float currentPercentageOfLandUsed = 0;

    /// <summary>
    /// Value corresponding to land use with 100% proteins-beef diet (20% beef overall)
    /// </summary>
    public float
        worstAmountOfLandUsed = 248449.4f; 

    /// <summary>
    /// Refresh the entire data computation : current GHG, current water and current land used.
    /// Then, update the metaphors and refresh the output display.
    /// </summary>
    public void RefreshMiniWorldData()
    {
        currentAmountOfCO2 = GameManager.Instance.currentYear *
                             GameManager.Instance.worldAgribalyseDataComputerList[worldNumberAssociated].totalFoodGHGEmissionsInOneYear -
                             CO2SinkAnnualValue * GameManager.Instance.currentYear;

        // Prevent Dana from having negative quantity of CO2.
        // If so, makes the glaciers expand and the see level decrease.
        if (currentAmountOfCO2 <= 0)
        {
            currentAmountOfCO2 = 0;
        }

        currentAmountOfFreshWater = initialFreshWaterVolume -
                                    GameManager.Instance.currentYear * GameManager.Instance.worldAgribalyseDataComputerList[worldNumberAssociated]
                                        .totalFoodWaterUsedInOneYear +
                                    GameManager.Instance.currentYear * annualReplenishment;

        // Prevent Dana from having more water than its initial stock.
        if (currentAmountOfFreshWater >= initialFreshWaterVolume)
        {
            currentAmountOfFreshWater = initialFreshWaterVolume;
        }

        currentPercentageOfLandUsed =
            GameManager.Instance.worldAgribalyseDataComputerList[worldNumberAssociated].totalFoodLandUsedInOneYear / worstAmountOfLandUsed;

        // After updating Dana's data according to its specific properties, call for the mini-world update.
        GameManager.Instance.miniWorldManagerList[worldNumberAssociated].UpdateMiniWorld();

        // After updating Dana's data according to its specific properties, call for the output update.
        GameManager.Instance.dataOutputList[worldNumberAssociated].RefreshOutputValuesFromMiniWorld();
    }

    /// <summary>
    /// Reset the data computed to 0 (GHG, water and land)
    /// </summary>
    public void ResetMiniWorldDataValue()
    {
        currentAmountOfCO2 = 0;
        currentAmountOfFreshWater = initialFreshWaterVolume;
        currentPercentageOfLandUsed = 0;
    }
}