using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// Class managing the mini-world.
/// It contains data used in Unity to alter the world and updates
/// and refreshes the world thanks to already computed indicators.
/// </summary>
public class MiniWorldManager : MonoBehaviour
{
    [SerializeField] private int worldNumberAssociated = -1;

    /// <summary>
    /// TransformManager instance of the game.
    /// </summary>
    [FormerlySerializedAs("movementManager")] [SerializeField]
    private TransformManager transformManager;

    /// <summary>
    /// CO2 quantity required for one magnitude elevation of the sea or one magnitude melting of the glacier.
    /// 600 kg = 0.6 t => elevate of seeHeightChangeMagnitude on y axe
    /// </summary>
    private int CO2QuantityRequiredForOneMagnitudeElevation = 600;

    /// <summary>
    /// Magnitude of a see elevation.
    /// Could be freely adjusted in the future. 
    /// </summary>
    private float seeHeightChangeMagnitude = 0.05f;

    /// <summary>
    /// Magnitude of glacier melting. Is a percentage of shrink.
    /// Melt 6% each year in theory (scientific data). Change it because to fast.
    /// </summary>
    private float meltingGlacierPercentage = 0.012f;

    /// <summary>
    /// Expose lakeHeightManager to access data about max and min height.
    /// Bad practice è.é
    /// </summary>
    [SerializeField] private HeightManager lakeHeightManager = null;

    /// <summary>
    /// GameObject containing all the wheat field of the mini-world
    /// </summary>
    [SerializeField] private GameObject wheatFieldContainer = null;

    /// <summary>
    /// List of all the wheat fields of the mini-world
    /// </summary>
    private List<Transform> wheatFieldList = new List<Transform>();
    
    /// <summary>
    /// GameObject containing all the tree modeling the forest in the mini-world
    /// </summary>
    [SerializeField] private GameObject forestContainer = null;

    /// <summary>
    /// List containg all the trees' transform
    /// </summary>
    private List<ForestManager> treeForestManagerList = new List<ForestManager>();


    /// <summary>
    /// Custom init function replace start and allow to be called
    /// in order in the Game Manager
    /// </summary>
    public void Init()
    {
        // Get all the wheat-field from the container, stock them in the list
        foreach (Transform wheatFieldTransform in wheatFieldContainer.GetComponentsInChildren<Transform>(true))
        {
            wheatFieldList.Add(wheatFieldTransform);
        }
        
        // Get all the trees from the container, stock them in the list
        foreach (ForestManager treeTrasnform in forestContainer.GetComponentsInChildren<ForestManager>(true))
        {
            treeForestManagerList.Add(treeTrasnform);
        }
    }


    /*********************
     * Update simulation *
     ********************/

    /// <summary>
    /// Update the whole simulation by updating all metaphors.
    /// </summary>
    public void UpdateMiniWorld()
    {
        UpdateCO2Consequences();
        UpdateLandConsequences();
        UpdateWaterConsequences();
    }

    /// <summary>
    /// Update metaphors related to CO2 indicator.
    /// </summary>
    private void UpdateCO2Consequences()
    {
        UpdateSeeLevel();
        UpdateIceMelting();
    }

    /// <summary>
    /// Update see level metaphor.
    /// </summary>
    private void UpdateSeeLevel()
    {
        double seeElevationNumber = GameManager.Instance.miniWorldDataConverterList[worldNumberAssociated].CurrentAmountOfCO2 /
                                    CO2QuantityRequiredForOneMagnitudeElevation;
        int nbOfElevationRequired = (int)Math.Round(seeElevationNumber);
        transformManager.SetSeeLevel(nbOfElevationRequired * seeHeightChangeMagnitude);
    }

    /// <summary>
    /// Update ice melting metaphor.
    /// </summary>
    private void UpdateIceMelting()
    {
        double percentageMeltingNumber = GameManager.Instance.miniWorldDataConverterList[worldNumberAssociated].CurrentAmountOfCO2 /
                                         CO2QuantityRequiredForOneMagnitudeElevation;
        transformManager.SetGlaciers((float)Math.Pow(1 - meltingGlacierPercentage, percentageMeltingNumber));
    }

    /// <summary>
    /// Update metaphors related to fresh water indicator.
    /// </summary>
    private void UpdateWaterConsequences()
    {
        float initialFreshWaterVolume = GameManager.Instance.miniWorldDataConverterList[worldNumberAssociated].initialFreshWaterVolume;
        float waterFillPercentage = GameManager.Instance.miniWorldDataConverterList[worldNumberAssociated].CurrentAmountOfFreshWater /
                                    initialFreshWaterVolume;
        float newHeight = (lakeHeightManager.GetMaxHeight() - lakeHeightManager.GetMinHeight()) * waterFillPercentage +
                          lakeHeightManager.GetMinHeight();
        transformManager.SetLakeLevel(newHeight);
    }

    /// <summary>
    /// Update metaphors related to land use indicator.
    /// </summary>
    private void UpdateLandConsequences()
    {
        double floatOfWheatToDisplay = wheatFieldList.Count *
                                       GameManager.Instance.miniWorldDataConverterList[worldNumberAssociated].CurrentPercentageOfLandUsed;
        int nbOfWheatToDisplay = (int)Math.Round(floatOfWheatToDisplay);
        SetWheatActive(nbOfWheatToDisplay);
    }

    /// <summary>
    /// Display a specific number of wheat on the mini-world.
    /// </summary>
    /// <param name="nbOfWheatToDisplay">Number of wheat to display</param>
    private void SetWheatActive(int nbOfWheatToDisplay)
    {
        // First, unable all the wheat 
        foreach (Transform wheatField in wheatFieldList)
        {
            wheatField.gameObject.SetActive(false);
        }

        // Then, enable all the trees
        foreach (ForestManager currentForestManager in treeForestManagerList)
        {
            currentForestManager.SetTreeActive();
        }
        
        // Check if we have more than 20% of beef in the percentage : it would be more than 100% of the wheat and 
        // overflow the list
        int realNbOfWheatToDisplay = Math.Clamp(nbOfWheatToDisplay, 0, wheatFieldList.Count);

        // Then, enable the right amount of wheat
        for (int i = 0; i < realNbOfWheatToDisplay; i++)
        {
            wheatFieldList[i].gameObject.SetActive(true);
        }
        
        // And unable trees connected to current activated wheat
        foreach (ForestManager currentForestManager in treeForestManagerList)
        {
            currentForestManager.HideTreeIfWheatIsActive();
        }
        
    }
}