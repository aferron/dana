using UnityEngine;

/// <summary>
/// Class managing the height of a game object through its transform.
/// </summary>
public class HeightManager : MonoBehaviour
{
    /// <summary>
    /// transform of the object to move
    /// </summary>
    [SerializeField] private Transform goTransform = null;
    
    /// <summary>
    /// initial position of the object
    /// </summary>
    private Vector3 initialPosition       = Vector3.zero;
    public float   initialHeightPosition = 0;
    
    /// <summary>
    /// maximum height
    /// </summary>
    [SerializeField] private float posMax = 0;
    
    /// <summary>
    /// minimum height
    /// </summary>
    [SerializeField] private float posMin = 0;
    
    /// <summary>
    /// granularity of the movement 
    /// </summary>
    [SerializeField] private float movementStep = 0;


    void Awake()
    {
        initialPosition = goTransform.localPosition;
        initialHeightPosition = initialPosition.y;
    }

    /// <summary>
    /// Resets object position to its initial location.
    /// </summary>
    public void ResetPosition()
    {
        goTransform.localPosition = initialPosition;
    }
    
    /// <summary>
    /// Increase height by movementStep multiplied by the magnitude given
    /// </summary>
    /// <param name="magnitude">Magnitude of one step</param>
    public void IncreaseHeight(float magnitude)
    {
        // Local copy of the position
        Vector3 futurePosition = goTransform.localPosition;
        futurePosition.y += movementStep * magnitude;
        // Update position
        if (IsNewPositionValid(futurePosition))
        {
            goTransform.localPosition = futurePosition;
        }
    }
    
    /// <summary>
    /// Decrease height by movementStep multiplied by the magnitude given.
    /// </summary>
    /// <param name="magnitude">Magnitude of one step</param>
    public void DecreaseHeight(float magnitude)
    {
        // Local copy of the position
        Vector3 futurePosition = goTransform.localPosition;
        futurePosition.y -= movementStep * magnitude;
        // Update position
        if (IsNewPositionValid(futurePosition))
        {
            goTransform.localPosition = futurePosition;
        }
    }

    /// <summary>
    /// Set new height to the gameObject
    /// </summary>
    /// <param name="newPosition">New height of the gameObject</param>
    public void SetHeight(float newPosition)
    {
        Vector3 futurePosition = goTransform.localPosition;
        futurePosition.y = newPosition;
        if (IsNewPositionValid(futurePosition))
        {
            goTransform.localPosition = futurePosition;
        }
    }

    /// <summary>
    /// Checks if the position is valid according to the max and the min height allowed.
    /// </summary>
    /// <param name="newPosition">New position to test the validity.</param>
    /// <returns>True if its valid, false otherwise.</returns>
    private bool IsNewPositionValid(Vector3 newPosition)
    {
        if (newPosition.y < posMin)
        {
            SetOnMin();
            return false;

        }
        else if (newPosition.y > posMax)
        {
            SetOnMax();
            return false;
        }
        return true; 
    }

    /// <summary>
    /// Set gameObject to its minimum position.
    /// </summary>
    public void SetOnMin()
    {
        Vector3 futurePosition = goTransform.localPosition;
        futurePosition.y = posMin;
        goTransform.localPosition = futurePosition;
    }
    
    /// <summary>
    /// Set gameObject to its maximum position.
    /// </summary>
    public void SetOnMax()
    {
        Vector3 futurePosition = goTransform.localPosition;
        futurePosition.y = posMax;
        goTransform.localPosition = futurePosition;
    }

    /// <summary>
    /// Returns the max height position allowed.
    /// </summary>
    /// <returns></returns>
    public float GetMaxHeight()
    {
        return posMax;
    }
    
    /// <summary>
    /// Returns the min height position allowed.
    /// </summary>
    /// <returns></returns>
    public float GetMinHeight()
    {
        return posMin;
    }
}