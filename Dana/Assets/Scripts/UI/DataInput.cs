using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class managing the input in the UI.
/// </summary>
public class DataInput : MonoBehaviour
{
    /*****************************************
     * Pro view : detailed percentage inputs *
     ****************************************/

    /// <summary>
    /// Collection containing all the input fields of the food.
    /// </summary>
    private Dictionary<FoodEnum, TMP_InputField> foodInputFields = new Dictionary<FoodEnum, TMP_InputField>();

    /// <summary>
    /// Input field containing the percentage of beef eaten.
    /// </summary>
    [SerializeField] private TMP_InputField beefInputField = null;

    /// <summary>
    /// Input field containing the percentage of pork eaten.
    /// </summary>
    [SerializeField] private TMP_InputField porkInputField = null;

    /// <summary>
    /// Input field containing the percentage of fish eaten.
    /// </summary>
    [SerializeField] private TMP_InputField fishInputField = null;

    /// <summary>
    /// Input field containing the percentage of vegetables eaten.
    /// </summary>
    [SerializeField] private TMP_InputField vegetablesInputField = null;

    /// <summary>
    /// Input field containing the percentage of fruits eaten.
    /// </summary>
    [SerializeField] private TMP_InputField fruitsInputField = null;

    /// <summary>
    /// Input field containing the percentage of plant-based proteins eaten.
    /// </summary>
    [SerializeField] private TMP_InputField plantBasedProtInputField = null;

    /// <summary>
    /// Input field containing the percentage of starch/pasta eaten.
    /// </summary>
    [SerializeField] private TMP_InputField pastaInputField = null;

    /// <summary>
    /// Input field containing the percentage of chicken eaten.
    /// </summary>
    [SerializeField] private TMP_InputField chickenInputField = null;

    /// <summary>
    /// Input field containing the percentage of dairy eaten.
    /// </summary>
    [SerializeField] private TMP_InputField dairyInputField = null;

    /// <summary>
    /// Sum of the above diet percentage.
    /// </summary>
    private int percentageSum = 0;

    /// <summary>
    /// Validation text which gives a feedback regarding the current percentage sum.
    /// </summary>
    [SerializeField] private TMP_Text validationText = null;

    /***********************************************
     * Pro view : detailed menus ; proteins detail * 
     **********************************************/

    /// <summary>
    /// List containing all the proteins dropdown.
    /// Allow to browse them easily. 
    /// </summary>
    private ArrayList protInputDropdown = new ArrayList();

    /// <summary>
    /// Proteins dropdown : today's lunch
    /// </summary>
    [SerializeField] private TMP_Dropdown mondayLunchProt = null;

    /// <summary>
    /// Proteins dropdown : today's diner 
    /// </summary>
    [SerializeField] private TMP_Dropdown mondayDinerProt = null;

    /// <summary>
    /// Proteins dropdown : day-1 lunch
    /// </summary>
    [SerializeField] private TMP_Dropdown tuesdayLunchProt = null;

    /// <summary>
    /// Proteins dropdown : day-1 diner
    /// </summary>
    [SerializeField] private TMP_Dropdown tuesdayDinerProt = null;

    /// <summary>
    /// Proteins dropdown : day-2 lunch
    /// </summary>
    [SerializeField] private TMP_Dropdown wednesdayLunchProt = null;

    /// <summary>
    /// Proteins dropdown : day-2 diner
    /// </summary>
    [SerializeField] private TMP_Dropdown wednesdayDinerProt = null;

    /// <summary>
    /// Proteins dropdown : day-3 lunch
    /// </summary>
    [SerializeField] private TMP_Dropdown thursdayLunchProt = null;

    /// <summary>
    /// Proteins dropdown : day-3 diner
    /// </summary>
    [SerializeField] private TMP_Dropdown thursdayDinerProt = null;

    /// <summary>
    /// Proteins dropdown : day-4 lunch
    /// </summary>
    [SerializeField] private TMP_Dropdown fridayLunchProt = null;

    /// <summary>
    /// Proteins dropdown : day-4 diner
    /// </summary>
    [SerializeField] private TMP_Dropdown fridayDinerProt = null;

    /************************************************
    * Pro view : detailed menus ; vegetables detail * 
    ************************************************/

    /// <summary>
    /// List containing all the vegetables checkboxes.
    /// Allow to browse them easily. 
    /// </summary>
    private ArrayList vegetablesToggle = new ArrayList();

    [SerializeField] private Toggle mondayLunchVegetables    = null;
    [SerializeField] private Toggle mondayDinerVegetables    = null;
    [SerializeField] private Toggle tuesdayLunchVegetables   = null;
    [SerializeField] private Toggle tuesdayDinerVegetables   = null;
    [SerializeField] private Toggle wednesdayLunchVegetables = null;
    [SerializeField] private Toggle wednesdayDinerVegetables = null;
    [SerializeField] private Toggle thursdayLunchVegetables  = null;
    [SerializeField] private Toggle thursdayDinerVegetables  = null;
    [SerializeField] private Toggle fridayLunchVegetables    = null;
    [SerializeField] private Toggle fridayDinerVegetables    = null;


    /********************************************
    * Pro view : detailed menus ; fruits detail * 
    ********************************************/

    /// <summary>
    /// List containing all the fruits checkboxes.
    /// Allow to browse them easily. 
    /// </summary>
    private ArrayList fruitsToggle = new ArrayList();

    [SerializeField] private Toggle mondayLunchFruits    = null;
    [SerializeField] private Toggle mondayDinerFruits    = null;
    [SerializeField] private Toggle tuesdayLunchFruits   = null;
    [SerializeField] private Toggle tuesdayDinerFruits   = null;
    [SerializeField] private Toggle wednesdayLunchFruits = null;
    [SerializeField] private Toggle wednesdayDinerFruits = null;
    [SerializeField] private Toggle thursdayLunchFruits  = null;
    [SerializeField] private Toggle thursdayDinerFruits  = null;
    [SerializeField] private Toggle fridayLunchFruits    = null;
    [SerializeField] private Toggle fridayDinerFruits    = null;


    /********************************************
    * Pro view : detailed menus ; starch detail * 
    ********************************************/

    /// <summary>
    /// List containing all the starch checkboxes.
    /// Allow to browse them easily. 
    /// </summary>
    private ArrayList startchToggle = new ArrayList();

    [SerializeField] private Toggle mondayLunchStarch    = null;
    [SerializeField] private Toggle mondayDinerStarch    = null;
    [SerializeField] private Toggle tuesdayLunchStarch   = null;
    [SerializeField] private Toggle tuesdayDinerStarch   = null;
    [SerializeField] private Toggle wednesdayLunchStarch = null;
    [SerializeField] private Toggle wednesdayDinerStarch = null;
    [SerializeField] private Toggle thursdayLunchStarch  = null;
    [SerializeField] private Toggle thursdayDinerStarch  = null;
    [SerializeField] private Toggle fridayLunchStarch    = null;
    [SerializeField] private Toggle fridayDinerStarch    = null;


    /*******************************************
    * Pro view : detailed menus ; dairy detail * 
    *******************************************/

    /// <summary>
    /// List containing all the dairy checkboxes.
    /// Allow to browse them easily. 
    /// </summary>
    private ArrayList dairyToggle = new ArrayList();

    [SerializeField] private Toggle mondayLunchDairy    = null;
    [SerializeField] private Toggle mondayDinerDairy    = null;
    [SerializeField] private Toggle tuesdayLunchDairy   = null;
    [SerializeField] private Toggle tuesdayDinerDairy   = null;
    [SerializeField] private Toggle wednesdayLunchDairy = null;
    [SerializeField] private Toggle wednesdayDinerDairy = null;
    [SerializeField] private Toggle thursdayLunchDairy  = null;
    [SerializeField] private Toggle thursdayDinerDairy  = null;
    [SerializeField] private Toggle fridayLunchDairy    = null;
    [SerializeField] private Toggle fridayDinerDairy    = null;

    /*******************
     * Time management *
     ******************/
    [SerializeField] private Slider timeSlider = null;

    /// <summary>
    /// Function initialing the script.
    /// Allow inputs to be correctly linked in time to be read for the GameManager start. 
    /// </summary>
    public void Init()
    {
        foodInputFields.Add(FoodEnum.Beef, beefInputField);
        foodInputFields.Add(FoodEnum.Pork, porkInputField);
        foodInputFields.Add(FoodEnum.Fish, fishInputField);
        foodInputFields.Add(FoodEnum.Vegetables, vegetablesInputField);
        foodInputFields.Add(FoodEnum.Fruits, fruitsInputField);
        foodInputFields.Add(FoodEnum.PlantBasedProteins, plantBasedProtInputField);
        foodInputFields.Add(FoodEnum.Pasta, pastaInputField);
        foodInputFields.Add(FoodEnum.Chicken, chickenInputField);
        foodInputFields.Add(FoodEnum.Dairy, dairyInputField);

        protInputDropdown.Add(mondayLunchProt);
        protInputDropdown.Add(mondayDinerProt);
        protInputDropdown.Add(tuesdayLunchProt);
        protInputDropdown.Add(tuesdayDinerProt);
        protInputDropdown.Add(wednesdayDinerProt);
        protInputDropdown.Add(wednesdayLunchProt);
        protInputDropdown.Add(thursdayDinerProt);
        protInputDropdown.Add(thursdayLunchProt);
        protInputDropdown.Add(fridayDinerProt);
        protInputDropdown.Add(fridayLunchProt);

        dairyToggle.Add(mondayDinerDairy);
        dairyToggle.Add(mondayLunchDairy);
        dairyToggle.Add(tuesdayDinerDairy);
        dairyToggle.Add(tuesdayLunchDairy);
        dairyToggle.Add(wednesdayDinerDairy);
        dairyToggle.Add(wednesdayLunchDairy);
        dairyToggle.Add(thursdayDinerDairy);
        dairyToggle.Add(thursdayLunchDairy);
        dairyToggle.Add(fridayDinerDairy);
        dairyToggle.Add(fridayLunchDairy);

        vegetablesToggle.Add(mondayDinerVegetables);
        vegetablesToggle.Add(mondayLunchVegetables);
        vegetablesToggle.Add(tuesdayDinerVegetables);
        vegetablesToggle.Add(tuesdayLunchVegetables);
        vegetablesToggle.Add(wednesdayDinerVegetables);
        vegetablesToggle.Add(wednesdayLunchVegetables);
        vegetablesToggle.Add(thursdayDinerVegetables);
        vegetablesToggle.Add(thursdayLunchVegetables);
        vegetablesToggle.Add(fridayDinerVegetables);
        vegetablesToggle.Add(fridayLunchVegetables);

        fruitsToggle.Add(mondayDinerFruits);
        fruitsToggle.Add(mondayLunchFruits);
        fruitsToggle.Add(tuesdayDinerFruits);
        fruitsToggle.Add(tuesdayLunchFruits);
        fruitsToggle.Add(wednesdayDinerFruits);
        fruitsToggle.Add(wednesdayLunchFruits);
        fruitsToggle.Add(thursdayDinerFruits);
        fruitsToggle.Add(thursdayLunchFruits);
        fruitsToggle.Add(fridayDinerFruits);
        fruitsToggle.Add(fridayLunchFruits);

        startchToggle.Add(mondayDinerStarch);
        startchToggle.Add(mondayLunchStarch);
        startchToggle.Add(tuesdayDinerStarch);
        startchToggle.Add(tuesdayLunchStarch);
        startchToggle.Add(wednesdayDinerStarch);
        startchToggle.Add(wednesdayLunchStarch);
        startchToggle.Add(thursdayDinerStarch);
        startchToggle.Add(thursdayLunchStarch);
        startchToggle.Add(fridayDinerStarch);
        startchToggle.Add(fridayLunchStarch);

        SetAllProtToVG();
    }

    /// <summary>
    /// Entry point of the entire simulation.
    /// Get the inputs and trigger all the computation and the mini-world updates.
    /// </summary>
    public void GetInputsAndComputeData()
    {
        CheckPercentageValidity();
        // If the diet repartition proposed by the user exceed 100% percent, the computation can't be relevant
        if (CheckInputValidity())
        {
            GameManager.Instance.worldAgribalyseDataComputerList[0].ComputeFoodInput();


            GameManager.Instance.worldAgribalyseDataComputerList[1].ComputeFoodInput();
        }
    }


    /// <summary>
    /// Checks if the inputs sum is equal to 100% and display message accordingly
    /// </summary>
    public void CheckPercentageValidity()
    {
        bool inputValidity = CheckInputValidity();
        if (inputValidity)
        {
            validationText.text = "Sum is less or equal to 100% \n (sum = " + percentageSum + ")";
            validationText.color = Color.green;
        }
        else
        {
            validationText.text = "Sum exceeds 100% \n (sum = " + percentageSum + ") \n Computation is ignored.";
            validationText.color = Color.red;
        }
    }

    /// <summary>
    /// Compute the sum of the percentage and return if it is equal or less to 100%. Update the
    /// GameManager flag accordingly.
    /// </summary>
    /// <returns>True if the sum entered is less than or equal to 100%</returns>
    private bool CheckInputValidity()
    {
        int sum = 0;
        foreach (KeyValuePair<FoodEnum, TMP_InputField> foodInput in foodInputFields)
        {
            int currentValue = Int32.Parse(foodInput.Value.text);
            sum += currentValue;
            if (sum > 100)
            {
                percentageSum = sum;
                return false;
            }
        }

        percentageSum = sum;
        return true;
    }

    /// <summary>
    /// Returns percentage input associated to a kind of food.
    /// Returns -1 if keyFood does not match any input.
    /// </summary>
    /// <param name="keyFood">Food key allowing to get associated percentage</param>
    /// <returns>Percentage associated to the food, or -1 if key food is unknown.</returns>
    public int GetPercentageWithKey(FoodEnum keyFood)
    {
        TMP_InputField seekedInputField = null;
        foodInputFields.TryGetValue(keyFood, out seekedInputField);
        if (seekedInputField != null)
        {
            return Int32.Parse(seekedInputField.text);
        }

        Debug.LogWarning("KeyFood is unknown.");
        return -1;
    }

    /// <summary>
    /// Compute percentage with the detailed menu view and write it down in the percentage view.
    /// </summary>
    public void UpdatePercentagesWithMenu()
    {
        int chickenPortionNb = 0;
        int beefPortionNb = 0;
        int fishPortionNb = 0;
        int plantBasedProtPortionNb = 0;
        int pastaPortionNb = 0;
        int porkPortionNb = 0;
        int fruitsPortionNb = 0;
        int vegetablesPortionNb = 0;
        int dairyPortionNb = 0;

        foreach (TMP_Dropdown dropdown in protInputDropdown)
        {
            switch (dropdown.captionText.text)
            {
                case "Plant-based":
                    plantBasedProtPortionNb++;
                    break;
                case "Beef":
                    beefPortionNb++;
                    break;
                case "Pork":
                    porkPortionNb++;
                    break;
                case "Fish":
                    fishPortionNb++;
                    break;
                case "Chicken":
                    chickenPortionNb++;
                    break;
                default:
                    break;
            }
        }

        foreach (Toggle toggle in dairyToggle)
        {
            if (toggle.isOn)
            {
                dairyPortionNb++;
            }
        }

        foreach (Toggle toggle in vegetablesToggle)
        {
            if (toggle.isOn)
            {
                vegetablesPortionNb++;
            }
        }

        foreach (Toggle toggle in fruitsToggle)
        {
            if (toggle.isOn)
            {
                fruitsPortionNb++;
            }
        }

        foreach (Toggle toggle in startchToggle)
        {
            if (toggle.isOn)
            {
                pastaPortionNb++;
            }
        }

        int beePercentage = (int)(Math.Round(((beefPortionNb                     / 50f) * 100)));
        int porkPercentage = (int)(Math.Round(((porkPortionNb                    / 50f) * 100)));
        int protVegetablePercentage = (int)(Math.Round(((plantBasedProtPortionNb / 50f) * 100)));
        int chickenPercentage = (int)(Math.Round(((chickenPortionNb              / 50f) * 100)));
        int fishPercentage = (int)(Math.Round(((fishPortionNb                    / 50f) * 100)));
        int dairyPercentage = (int)(Math.Round(((dairyPortionNb                  / 50f) * 100)));
        int starchPercentage = (int)(Math.Round(((pastaPortionNb                 / 50f) * 100)));
        int fruitsPercentage = (int)(Math.Round(((fruitsPortionNb                / 50f) * 100)));
        int vegetablesPercentage = (int)(Math.Round(((vegetablesPortionNb        / 50f) * 100)));

        beefInputField.text = beePercentage.ToString();
        porkInputField.text = porkPercentage.ToString();
        plantBasedProtInputField.text = protVegetablePercentage.ToString();
        fishInputField.text = fishPercentage.ToString();
        chickenInputField.text = chickenPercentage.ToString();
        dairyInputField.text = dairyPercentage.ToString();
        vegetablesInputField.text = vegetablesPercentage.ToString();
        fruitsInputField.text = fruitsPercentage.ToString();
        pastaInputField.text = starchPercentage.ToString();
    }

    /// <summary>
    /// Set all the portion side to 20%, which conceptually correspond to
    /// eating one potion of dairy, fruits, vegetables and starch during a meal.
    /// </summary>
    private void SetSidePortionToOn()
    {
        dairyInputField.text = "20";
        vegetablesInputField.text = "20";
        fruitsInputField.text = "20";
        pastaInputField.text = "20";
    }

    /// <summary>
    /// Set the diet repartition on beef proteins only.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToBeef()
    {
        SetSidePortionToOn();
        beefInputField.text = "20";
        porkInputField.text = "0";
        plantBasedProtInputField.text = "0";
        fishInputField.text = "0";
        chickenInputField.text = "0";
    }

    /// <summary>
    /// Set the diet repartition on chicken proteins only.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToChicken()
    {
        SetSidePortionToOn();
        beefInputField.text = "0";
        porkInputField.text = "0";
        plantBasedProtInputField.text = "0";
        fishInputField.text = "0";
        chickenInputField.text = "20";
    }

    /// <summary>
    /// Set the diet repartition on fish proteins only.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToFish()
    {
        SetSidePortionToOn();
        beefInputField.text = "0";
        porkInputField.text = "0";
        plantBasedProtInputField.text = "0";
        fishInputField.text = "20";
        chickenInputField.text = "0";
    }

    /// <summary>
    /// Set the diet repartition on plant-based proteins only.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToVG()
    {
        SetSidePortionToOn();
        beefInputField.text = "0";
        porkInputField.text = "0";
        plantBasedProtInputField.text = "20";
        fishInputField.text = "0";
        chickenInputField.text = "0";
    }

    /// <summary>
    /// Set the diet repartition on pork proteins only.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToPork()
    {
        SetSidePortionToOn();
        beefInputField.text = "0";
        porkInputField.text = "20";
        plantBasedProtInputField.text = "0";
        fishInputField.text = "0";
        chickenInputField.text = "0";
    }

    /// <summary>
    /// Set the diet repartition equally between all the proteins only.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToMixed()
    {
        SetSidePortionToOn();
        beefInputField.text = "4";
        porkInputField.text = "4";
        plantBasedProtInputField.text = "4";
        fishInputField.text = "4";
        chickenInputField.text = "4";
    }

    /// <summary>
    /// Set the diet repartition equally between all the proteins except beef.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToNoBeef()
    {
        SetSidePortionToOn();
        beefInputField.text = "0";
        porkInputField.text = "5";
        plantBasedProtInputField.text = "5";
        fishInputField.text = "5";
        chickenInputField.text = "5";
    }

    /// <summary>
    /// Set the diet repartition equally between all the proteins except beef and pork.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToNoBeefNoPork()
    {
        SetSidePortionToOn();
        beefInputField.text = "0";
        porkInputField.text = "0";
        plantBasedProtInputField.text = "7";
        fishInputField.text = "6";
        chickenInputField.text = "7";
    }

    /// <summary>
    /// Set the diet repartition equally between chicken and plant-based proteins.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToChickenAndVG()
    {
        SetSidePortionToOn();
        beefInputField.text = "0";
        porkInputField.text = "0";
        plantBasedProtInputField.text = "10";
        fishInputField.text = "0";
        chickenInputField.text = "10";
    }

    /// <summary>
    /// Set the diet repartition equally between fish and plant-based proteins.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToFishAndVG()
    {
        SetSidePortionToOn();
        beefInputField.text = "0";
        porkInputField.text = "0";
        plantBasedProtInputField.text = "10";
        fishInputField.text = "10";
        chickenInputField.text = "0";
    }

    /// <summary>
    /// Set the diet repartition equally between beef and plant-based proteins.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToBeefAndVG()
    {
        SetSidePortionToOn();
        beefInputField.text = "10";
        porkInputField.text = "0";
        plantBasedProtInputField.text = "10";
        fishInputField.text = "0";
        chickenInputField.text = "0";
    }

    /// <summary>
    /// Set the diet repartition equally between pork and plant-based proteins.
    /// Assume side portions is required and activate them.
    /// </summary>
    public void SetAllProtToPorkAndVG()
    {
        SetSidePortionToOn();
        beefInputField.text = "0";
        porkInputField.text = "10";
        plantBasedProtInputField.text = "10";
        fishInputField.text = "0";
        chickenInputField.text = "0";
    }

    /// <summary>
    /// Update time value in the software by getting the UI slider value.
    /// Then, refresh mini-world data accordingly.
    /// </summary>
    public void GetTimeSliderValue()
    {
        GameManager.Instance.currentYear = (int)timeSlider.value;
        GetInputsAndComputeData();
    }

    /// <summary>
    /// Resets all input values to 0, i.e. percentages and time.
    /// </summary>
    private void ResetInputValues()
    {
        beefInputField.text = "0";
        porkInputField.text = "0";
        plantBasedProtInputField.text = "0";
        fishInputField.text = "0";
        chickenInputField.text = "0";
        dairyInputField.text = "0";
        vegetablesInputField.text = "0";
        fruitsInputField.text = "0";
        pastaInputField.text = "0";
        timeSlider.value = 0;
    }

    /// <summary>
    /// Resets all the software values in each modules. Then, refresh the mini-world accordingly. 
    /// </summary>
    public void ResetAllValues()
    {
        ResetInputValues();
        GameManager.Instance.worldAgribalyseDataComputerList[0].ResetDataValueComputation();
        GameManager.Instance.worldAgribalyseDataComputerList[1].ResetDataValueComputation();
        GameManager.Instance.miniWorldDataConverterList[0].ResetMiniWorldDataValue();
        GameManager.Instance.miniWorldDataConverterList[1].ResetMiniWorldDataValue();
        GameManager.Instance.miniWorldManagerList[0].UpdateMiniWorld();
        GameManager.Instance.miniWorldManagerList[1].UpdateMiniWorld();
        GameManager.Instance.dataOutputList[0].RefreshOutputValuesFromAgribalyse();
        GameManager.Instance.dataOutputList[1].RefreshOutputValuesFromAgribalyse();
    }
}