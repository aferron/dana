using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ExplanationUIManager : MonoBehaviour
{
    private int _currentExplanationStep = 0;

    private Dictionary<ExplanationEnum, ExplanationScript> _explanationList =
        new Dictionary<ExplanationEnum, ExplanationScript>();

    [SerializeField] private TMP_Text currentTitle = null;
    [SerializeField] private TMP_Text currentBody = null;

    [SerializeField] private GameObject explanationUI = null;
    [SerializeField] private GameObject mainUI        = null;
    

    // Start is called before the first frame update
    void Start()
    {
        List<ExplanationScript> extractedExplanationScript = new List<ExplanationScript>();
        gameObject.GetComponents<ExplanationScript>(extractedExplanationScript);
        foreach (ExplanationScript explanationScript in extractedExplanationScript)
        {
            _explanationList.Add(explanationScript.myExplanationEnum, explanationScript);
        }
        UpdateOutput();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void UpdateOutput()
    {
        if (_currentExplanationStep < 0 || _currentExplanationStep >= (int) ExplanationEnum.Unknown)
        {
            ResetDisplay();
            DisplayMainUI();
            return;
        }
        
        ExplanationScript requiredExplanationScript = null;
        _explanationList.TryGetValue((ExplanationEnum)_currentExplanationStep, out requiredExplanationScript);
        if (requiredExplanationScript != null)
        {
            currentTitle.text = requiredExplanationScript.title;
            currentBody.text = requiredExplanationScript.body;
        }
    }

    public void NextExplanation()
    {
        int futureExplanationStep = Math.Clamp(_currentExplanationStep + 1, -1, (int)ExplanationEnum.Unknown);
        _currentExplanationStep = futureExplanationStep;
        UpdateOutput();
    }
    
    public void PastExplanation()
    {
        int futureExplanationStep = Math.Clamp(_currentExplanationStep - 1, -1, (int)ExplanationEnum.Unknown);
        _currentExplanationStep = futureExplanationStep;
        UpdateOutput();
    }

    public void DisplayExplanationUI()
    {
        explanationUI.SetActive(true);
        mainUI.SetActive(false);
    }
    
    public void DisplayMainUI()
    {
        explanationUI.SetActive(false);
        mainUI.SetActive(true);
    }

    private void ResetDisplay()
    {
        _currentExplanationStep = 0;
        ExplanationScript requiredExplanationScript = null;
        _explanationList.TryGetValue((ExplanationEnum)_currentExplanationStep, out requiredExplanationScript);
        if (requiredExplanationScript != null)
        {
            currentTitle.text = requiredExplanationScript.title;
            currentBody.text = requiredExplanationScript.body;
        }
    }
}
