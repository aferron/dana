/// <summary>
/// A food enum is created for each type of food used in the simulation.
/// If a new type of food is used in Dana, please add new enum corresponding. 
/// </summary>
public enum ExplanationEnum
{
    Introduction,
    Data,
    Computation,
    Modelling,
    Functionalities,

    // Add new Enum above if necessary.
    Unknown,
}