using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class managing the food percentage input in the UI.
/// </summary>
public class DataOutput : MonoBehaviour
{
    [SerializeField] private int worldNumberAssociated = -1;
    
    /*******************************************
     * Five-days Agribalyse computation output *
     ******************************************/
    [SerializeField] private TMP_Text landFiveDaysOutputField  = null;
    [SerializeField] private TMP_Text waterFiveDaysOutputField = null;
    [SerializeField] private TMP_Text GHGFiveDaysOutputField   = null;

    /******************************************
     * One-year Agribalyse computation output *
     *****************************************/
    [SerializeField] private TMP_Text landOneYearOutputField  = null;
    [SerializeField] private TMP_Text waterOneYearOutputField = null;
    [SerializeField] private TMP_Text GHGOneYearOutputField   = null;

    /*******************************
     * Percentage details feedback *
     ******************************/
    [SerializeField] private TMP_Text beefOutputField           = null;
    [SerializeField] private TMP_Text porkOutputField           = null;
    [SerializeField] private TMP_Text fishOutputField           = null;
    [SerializeField] private TMP_Text chickenOutputField        = null;
    [SerializeField] private TMP_Text fruitsOutputField         = null;
    [SerializeField] private TMP_Text vegetablesOutputField     = null;
    [SerializeField] private TMP_Text plantBasedProtOutputField = null;
    [SerializeField] private TMP_Text pastaOutputField          = null;
    [SerializeField] private TMP_Text dairyOutputField          = null;
    [SerializeField] private TMP_Text sumOutputField            = null;

    /**************************
     * Time slider management *
     *************************/
    [SerializeField] private Slider   timeSlider = null;
    [SerializeField] private TMP_Text timePrompt = null;

    /**************************
     * Mini-world data output *
     *************************/
    [SerializeField] private TMP_Text globalCO2Prompt   = null;
    [SerializeField] private TMP_Text globalWaterPrompt = null;
    [SerializeField] private TMP_Text globalLandPrompt  = null;
    [SerializeField] private TMP_Text gloablYearPrompt  = null;


    public void RefreshOutputValuesFromMiniWorld()
    {
        timePrompt.text = timeSlider.value.ToString();
        globalCO2Prompt.text = GameManager.Instance.miniWorldDataConverterList[worldNumberAssociated].CurrentAmountOfCO2.ToString();
        globalWaterPrompt.text = GameManager.Instance.miniWorldDataConverterList[worldNumberAssociated].CurrentAmountOfFreshWater.ToString();
        globalLandPrompt.text = GameManager.Instance.miniWorldDataConverterList[worldNumberAssociated].CurrentPercentageOfLandUsed.ToString();
        gloablYearPrompt.text = timeSlider.value.ToString();
    }

    public void RefreshOutputValuesFromAgribalyse()
    {
        landFiveDaysOutputField.text =
            GameManager.Instance.worldAgribalyseDataComputerList[worldNumberAssociated].totalFoodLandUsedInFiveDays.ToString();
        waterFiveDaysOutputField.text =
            GameManager.Instance.worldAgribalyseDataComputerList[worldNumberAssociated].totalFoodWaterUsedInFiveDays.ToString();
        GHGFiveDaysOutputField.text =
            GameManager.Instance.worldAgribalyseDataComputerList[worldNumberAssociated].totalFoodGHGEmissionsInFiveDays.ToString();
        landOneYearOutputField.text =
            GameManager.Instance.worldAgribalyseDataComputerList[worldNumberAssociated].totalFoodLandUsedInOneYear.ToString();
        waterOneYearOutputField.text =
            GameManager.Instance.worldAgribalyseDataComputerList[worldNumberAssociated].totalFoodWaterUsedInOneYear.ToString();
        GHGOneYearOutputField.text =
            GameManager.Instance.worldAgribalyseDataComputerList[worldNumberAssociated].totalFoodGHGEmissionsInOneYear.ToString();


        beefOutputField.text = GameManager.Instance.dataInputList[worldNumberAssociated].GetPercentageWithKey(FoodEnum.Beef)
            .ToString();
        porkOutputField.text = GameManager.Instance.dataInputList[worldNumberAssociated].GetPercentageWithKey(FoodEnum.Pork)
            .ToString();
        chickenOutputField.text = GameManager.Instance.dataInputList[worldNumberAssociated]
            .GetPercentageWithKey(FoodEnum.Chicken).ToString();
        fishOutputField.text = GameManager.Instance.dataInputList[worldNumberAssociated].GetPercentageWithKey(FoodEnum.Fish)
            .ToString();
        vegetablesOutputField.text = GameManager.Instance.dataInputList[worldNumberAssociated]
            .GetPercentageWithKey(FoodEnum.Vegetables).ToString();
        plantBasedProtOutputField.text = GameManager.Instance.dataInputList[worldNumberAssociated]
            .GetPercentageWithKey(FoodEnum.PlantBasedProteins).ToString();
        fruitsOutputField.text = GameManager.Instance.dataInputList[worldNumberAssociated].GetPercentageWithKey(FoodEnum.Fruits)
            .ToString();
        pastaOutputField.text = GameManager.Instance.dataInputList[worldNumberAssociated].GetPercentageWithKey(FoodEnum.Pasta)
            .ToString();
        dairyOutputField.text = GameManager.Instance.dataInputList[worldNumberAssociated].GetPercentageWithKey(FoodEnum.Dairy)
            .ToString();
        sumOutputField.text = GameManager.Instance.worldAgribalyseDataComputerList[worldNumberAssociated].currentSumPercentages.ToString();
    }
}