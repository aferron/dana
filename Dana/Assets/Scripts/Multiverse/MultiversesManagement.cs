using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class MultiversesManagement : MonoBehaviour
{
    [SerializeField] private GameObject firstMiniWorldCameras = null;

    private                  List<Camera> listOfFirstWorldCameras = new List<Camera>();
    [SerializeField] private GameObject   secondMiniWorld         = null;

    [SerializeField] private CamerasManager firstWorldCamerasManager = null;
    
    [SerializeField] private CamerasManager secondWorldCamerasManager = null;

    // Start is called before the first frame update
    void Start()
    {
        foreach (Camera currentCamera in firstMiniWorldCameras.GetComponentsInChildren<Camera>(true))
        {
            listOfFirstWorldCameras.Add(currentCamera);
        }
    }

    public void LaunchSecondWorld()
    {
        foreach (Camera currentCamera in listOfFirstWorldCameras)
        {
            currentCamera.rect = new Rect(0,0,0.5f,1);
        }
        
        secondMiniWorld.SetActive(true);
    }

    public void GetBackToOneWorld()
    {
        secondMiniWorld.SetActive(false);
        foreach (Camera currentCamera in listOfFirstWorldCameras)
        {
            currentCamera.rect =  new Rect(0,0,1,1);
        }
    }

    public void ShowMainView()
    {
        firstWorldCamerasManager.ShowMainView();
        if (secondMiniWorld.activeSelf)
        {
            secondWorldCamerasManager.ShowMainView();
        }
    }
    
    public void ShowBeach()
    {
        firstWorldCamerasManager.ShowBeach();
        if (secondMiniWorld.activeSelf)
        {
            secondWorldCamerasManager.ShowBeach();
        }
    }
    
    public void ShowForest()
    {
        firstWorldCamerasManager.ShowForest();
        if (secondMiniWorld.activeSelf)
        {
            secondWorldCamerasManager.ShowForest();
        }
    }
    
    public void ShowIsland()
    {
        firstWorldCamerasManager.ShowIsland();
        if (secondMiniWorld.activeSelf)
        {
            secondWorldCamerasManager.ShowIsland();
        }
    }
    
    public void ShowMountain()
    {
        firstWorldCamerasManager.ShowMountain();
        if (secondMiniWorld.activeSelf)
        {
            secondWorldCamerasManager.ShowMountain();
        }
    }
    
    public void ShowLakeVillage()
    {
        firstWorldCamerasManager.ShowLakeVillage();
        if (secondMiniWorld.activeSelf)
        {
            secondWorldCamerasManager.ShowLakeVillage();
        }
    }
}