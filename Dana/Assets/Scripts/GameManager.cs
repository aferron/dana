using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// This class acts as a singleton. It's the core of the software, containing
/// an instance of each important modules (data input, output, computation, simulation...)
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Instance of the GameManager singleton.
    /// </summary>
    public static GameManager Instance;

    /// <summary>
    /// DataInput instance of the game.
    /// </summary>
    public List<DataInput> dataInputList = new List<DataInput>();

    /// <summary>
    /// FoodDataCollection instance of the game. 
    /// </summary>
    public FoodDataCollection myFoodDataCollection = null;

    /// <summary>
    /// DataComputer list of the mini-worlds.
    /// </summary>
    public List<AgribalyseDataComputer> worldAgribalyseDataComputerList = new List<AgribalyseDataComputer>();

    /// <summary>
    /// Current year chosen by the player.
    /// </summary>
    public int currentYear = 0;

    /// <summary>
    /// SimulationDataConverter list of the mini-worlds.
    /// </summary>
    public List<MiniWorldDataConverter> miniWorldDataConverterList = new List<MiniWorldDataConverter>();

    /// <summary>
    /// DataOutput instance of the game.
    /// </summary>
    public List<DataOutput> dataOutputList = new List<DataOutput>();

    /// <summary>
    /// SimulationManager instance of the first mini-world.
    /// </summary>
    public List<MiniWorldManager> miniWorldManagerList = new List<MiniWorldManager>();

    /// <summary>
    /// Reference toward parameter's menu.
    /// </summary>
    [SerializeField] private GameObject parametersMenu = null;

    [SerializeField] private List<GameObject> gameObjectsToDeactivateOnLaunch = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        // Singleton pattern
        if (Instance != null && Instance != this)
            Destroy(gameObject);

        Instance = this;

        // Init data computer
        worldAgribalyseDataComputerList.Add(new AgribalyseDataComputer(0));
        worldAgribalyseDataComputerList.Add(new AgribalyseDataComputer(1));

        // Extract food data
        myFoodDataCollection.initFoodData();

        // Init miniWorldManager
        miniWorldManagerList[0].Init();
        miniWorldManagerList[1].Init();

        // Init inputValues
        dataInputList[0].Init();
        dataInputList[1].Init();


        // Init the entire game and outputs
        dataInputList[0].GetInputsAndComputeData();
        dataInputList[1].GetInputsAndComputeData();

        foreach (GameObject go in gameObjectsToDeactivateOnLaunch)
        {
            go.SetActive(false);
        }
    }

    private void Update()
    {
        // If Escape is pressed when parameters' menu is opened, quit Dana
        if (Input.GetKeyDown(KeyCode.Escape) && parametersMenu.activeSelf)
        {
            QuitGame();
        }

        // If Escape is pressed when playing Dana, display parameters' menu.
        if (Input.GetKeyDown(KeyCode.Escape) && !parametersMenu.activeSelf)
        {
            parametersMenu.SetActive(true);
        }
    }

    /// <summary>
    /// Close the application.
    /// </summary>
    public void QuitGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// Set the application display mode on windowed.
    /// </summary>
    public void SetWindowedOn()
    {
        Screen.fullScreen = false;
    }

    /// <summary>
    /// Set the application display mode on full screen.
    /// </summary>
    public void SetFullScreenOn()
    {
        Screen.fullScreen = true;
    }
}